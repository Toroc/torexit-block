# torexit-block

Script for blocking tor exit nodes using ipsets

# Usage

### Single run
```bash bin/torexit-block```

### Persist
```ansible-playbook setup.yml```